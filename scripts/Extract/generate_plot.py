import sys
import json
import numpy
from pprint import pprint

if __name__ == "__main__":
    
    if len(sys.argv) < 2:
        print "Lacking json file"

    else:

        filename = sys.argv[1]

        with open(filename) as data_file:    
            data = json.load(data_file)

        keys = data.keys()
        keys.sort()
        #print keys

        j = 0
        plot_file = open("temperature_plot", "w")
        for i in keys:
            a = data[i]
            s = "%d %f %f %s\n" % (j, a["mean_temperature"], a["std_temperature"], i)
            plot_file.write(s)
            j += 1
        plot_file.close()

        j = 0
        plot_file = open("humidity_plot", "w")
        for i in keys:
            a = data[i]
            s = "%d %f %f %s\n" % (j, a["mean_humidity"], a["std_humidity"], i)
            plot_file.write(s)
            j += 1
        plot_file.close()
            

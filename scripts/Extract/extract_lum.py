#imports
import sys
import json
from pprint import pprint
from datetime import datetime
from dateutil import tz

if __name__ == "__main__":
    
    if len(sys.argv) < 3:
        print "Lacking json file and destination file"

    else:

        filename = sys.argv[1]
        dest_filename = sys.argv[2]
        channel_data = {"data": []}

        from_zone = tz.gettz("UTC")
        to_zone = tz.gettz('America/Argentina/Buenos_Aires')
        
        with open(filename) as data_file:    
            data = json.load(data_file)


        data_list = data["feeds"]

        for i in range(len(data_list)):
            a = data_list[i]
            if a["field1"] == "nan":
                continue
            a["field1"] = int(a["field1"])
            a["field2"] = int(a["field2"])
            a["field3"] = int(a["field3"])
            a["field4"] = int(a["field4"])

            #correcting time according to timezone
            time = a["created_at"]
            utc = datetime.strptime(time, '%Y-%m-%dT%H:%M:%SZ')
            utc = utc.replace(tzinfo=from_zone)
            new_time = utc.astimezone(to_zone)
            time = new_time.strftime('%Y-%m-%dT%H:%M:%SZ')
            a["created_at"] = time

            channel_data["data"].append(a)

        print "Total entries: %d" % (len(channel_data["data"]))
        with open(dest_filename,"w") as dest_file:
            json.dump(channel_data, dest_file, sort_keys=True, indent=4, separators=(',',': '))
        #pprint(data)


#imports
import sys
import json
import numpy
from pprint import pprint

if __name__ == "__main__":
    
    if len(sys.argv) < 2:
        print "Lacking json file and destination file"

    else:

        filename = sys.argv[1]

        with open(filename) as data_file:    
            data = json.load(data_file)


        temp_hour = {}
        hum_hour = {}
        mean_temp_hour = {}
        mean_hum_hour = {}
        for i in data.keys():
            a = data[i]["hours"]["temperature"]
            for j in a.keys():
                b = a[j]
                for k in b:
                    try:
                        temp_hour[j].append(k)
                    except:
                        temp_hour[j] = []
                        temp_hour[j].append(k)

            a = data[i]["hours"]["humidity"]
            for j in a.keys():
                b = a[j]
                for k in b:
                    try:
                        hum_hour[j].append(k)
                    except:
                        hum_hour[j] = []
                        hum_hour[j].append(k)

        for i in temp_hour.keys():
            mean_temp = numpy.mean(temp_hour[i])
            mean_temp_hour[i] = mean_temp

        for i in hum_hour.keys():
            mean_hum = numpy.mean(hum_hour[i])
            mean_hum_hour[i] = mean_hum


        with open("mean_temp_hour.json","w") as dest_file:
            json.dump(mean_temp_hour, dest_file, sort_keys=True, indent=4, separators=(',',': '))
        dest_file.close()

        with open("mean_hum_hour.json","w") as dest_file:
            json.dump(mean_hum_hour, dest_file, sort_keys=True, indent=4, separators=(',',': '))
        dest_file.close()


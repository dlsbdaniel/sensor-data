#!/bin/bash

#script for collecting data from a thingspeak channel as a json file
channel_id=36620
channel_url="http://api.thingspeak.com/channels/$channel_id/status"
api_key='ZVDDM5YJWVMEXBL5'
now=`date +"%m-%d"`

#this one is specific for data about sensors. can be removed or changed
sensor='hum'

destination_file="data-$now-$sensor.json"
#getting data from thingspeak
curl -k -G --data "api_key=$api_key" $channel_url | json_reformat > $destination_file

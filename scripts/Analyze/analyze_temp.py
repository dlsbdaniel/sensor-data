#This is a script to extract data from the temperature and humidity collected 
#from sensors. The script calculates the standard deviation, variance, mean 
#and mode for each specific metric.

#imports
import sys
import json
import numpy
from pprint import pprint

if __name__ == "__main__":
    
    if len(sys.argv) < 2:
        print "Lacking json file"

    else:

        filename = sys.argv[1]

        with open(filename) as data_file:    
            data = json.load(data_file)


        data_list = data["data"]
        ocurrence_temp = {}
        ocurrence_hum = {}
        temp = []
        hum = []
        mean_temp = 0.0
        mean_hum = 0.0

        for i in range(len(data_list)):
            a = data_list[i]
            mean_temp += a["field1"]
            mean_hum += a["field2"]

            temp.append(a["field1"])
            hum.append(a["field2"])

            try:
                ocurrence_temp[a["field1"]] += 1
            except:
                ocurrence_temp[a["field1"]] = 1

            try:
                ocurrence_hum[a["field2"]] += 1
            except:
                ocurrence_hum[a["field2"]] = 1

        mean_temp = mean_temp / len(data_list)
        mean_hum = mean_hum / len(data_list)
        std_temp = numpy.std(temp)
        std_hum = numpy.std(hum)
        var_temp = numpy.var(temp)
        var_hum = numpy.var(hum)

        with open("ocurrence_temp.json","w") as dest_file:
            json.dump(ocurrence_temp, dest_file, sort_keys=True, indent=4, separators=(',',': '))
        dest_file.close()

        with open("ocurrence_hum.json","w") as dest_file:
            json.dump(ocurrence_hum, dest_file, sort_keys=True, indent=4, separators=(',',': '))
        dest_file.close()

                
        print "Mean Temperature: %f" % (mean_temp)
        print "Mean Humidity: %f" % (mean_hum)
        print "Temperature Standard Deviation: %f" % (std_temp)
        print "Humidity Standard Deviation: %f" % (std_hum)


        #Extracting temperature and humidity values for each day
        day_data = {}

        for i in range(len(data_list)):
            a = data_list[i]
            day = a["created_at"].split("T")[0]
            hour = a["created_at"].split("T")[1].split(":")[0] #God have mercy on my soul
            
            
            try:
                day_data[day]["temperature"].append(a["field1"])
                day_data[day]["humidity"].append(a["field2"])
            except:
                day_data[day] = {"temperature": [], "humidity": [], 
                                 "mean_temperature": 0.0, "mean_humidity": 0.0,
                                 "var_temperature": 0.0, "std_temperature": 0.0,
                                 "var_humidity": 0.0, "std_humidity": 0.0,
                                 "hours": {"temperature": {},
                                           "humidity": {}
                                          }
                                 }
                day_data[day]["temperature"].append(a["field1"])
                day_data[day]["humidity"].append(a["field2"])

            try:
                day_data[day]["hours"]["temperature"][hour].append(a["field1"])
                day_data[day]["hours"]["humidity"][hour].append(a["field2"])
            except:
                day_data[day]["hours"]["temperature"][hour] = []
                day_data[day]["hours"]["humidity"][hour] = []

                day_data[day]["hours"]["temperature"][hour].append(a["field1"])
                day_data[day]["hours"]["humidity"][hour].append(a["field2"])

        
        #Calculating Variance and Standard deviation of each day data
        for i in day_data.keys():
            a = day_data[i]
            var_temp = numpy.var(a["temperature"])
            std_temp = numpy.std(a["temperature"])

            var_hum = numpy.var(a["humidity"])
            std_hum = numpy.std(a["humidity"])

            mean_temp = numpy.mean(a["temperature"])
            mean_hum = numpy.mean(a["humidity"])
            
            day_data[i]["mean_temperature"] = mean_temp
            day_data[i]["mean_humidity"] = mean_hum
            day_data[i]["var_temperature"] = var_temp
            day_data[i]["std_temperature"] = std_temp
            day_data[i]["var_humidity"] = var_hum
            day_data[i]["std_humidity"] = std_hum


        with open("data_per_day_temp.json","w") as dest_file:
            json.dump(day_data, dest_file, sort_keys=True,indent=4, separators=(',',': '))
        dest_file.close()


        

#imports
import sys
import json
from pprint import pprint

if __name__ == "__main__":
    
    if len(sys.argv) < 2:
        print "Lacking json file and destination file"

    else:

        filename = sys.argv[1]

        with open(filename) as data_file:    
            data = json.load(data_file)


        data_list = data["data"]
        ocurrence_lum = {}
        lum = []

        for i in range(len(data_list)):
            a = data_list[i]
            lum.append(a["field2"])

            try:
                ocurrence_lum[a["field2"]] += 1
            except:
                ocurrence_lum[a["field2"]] = 1



        with open("ocurrence_lum.json","w") as dest_file:
            json.dump(ocurrence_lum, dest_file, sort_keys=True, indent=4, separators=(',',': '))
        dest_file.close()

        #Extracting temperature and humidity values for each day
        day_data = {}

        for i in range(len(data_list)):
            a = data_list[i]
            day = a["created_at"].split("T")[0]
            hour = a["created_at"].split("T")[1].split(":")[0] #God have mercy on my soul
            
            
            try:
                #day_data[day]["luminosity_a"].append(a["field1"])
                day_data[day]["luminosity"].append(a["field2"])
            except:
                day_data[day] = {"luminosity": [], 
                                 "hours": {"luminosity": {}}
                                }
                #day_data[day]["luminosity_a"].append(a["field1"])
                day_data[day]["luminosity"].append(a["field2"])

            try:
                #day_data[day]["hours"]["luminosity_a"][hour].append(a["field1"])
                day_data[day]["hours"]["luminosity"][hour].append(a["field2"])
            except:
                #day_data[day]["hours"]["luminosity_a"][hour] = []
                day_data[day]["hours"]["luminosity"][hour] = []

                #day_data[day]["hours"]["luminosity_a"][hour].append(a["field1"])
                day_data[day]["hours"]["luminosity"][hour].append(a["field2"])

        
        with open("data_per_day_lum.json","w") as dest_file:
            json.dump(day_data, dest_file, sort_keys=True,indent=4, separators=(',',': '))
        dest_file.close()


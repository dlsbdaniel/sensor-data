#imports
import sys
import json
import numpy
from pprint import pprint

if __name__ == "__main__":
    
    if len(sys.argv) < 2:
        print "Lacking json file"

    else:

        filename = sys.argv[1]

        with open(filename) as data_file:    
            data = json.load(data_file)


        data_list = data["data"]

        #Extracting temperature and humidity values for each day
        day_data = {}

        for i in range(len(data_list)):
            a = data_list[i]
            day = a["created_at"].split("T")[0]
            hour = a["created_at"].split("T")[1].split(":")[0] #God have mercy on my soul
            status = a["status"]
            user_index = status.find("User")
            status = status[user_index:len(status)]

            user = status.split(" ")[1]
            
            
            try:
                if user not in day_data[day]["users"]:
                    day_data[day]["users"].append(user)
            except:
                day_data[day] = {"users": [],
                                 "hours": {"users": {}}}
                day_data[day]["users"].append(user)

            try:
                day_data[day]["hours"]["users"][hour].append(user)
            except:
                day_data[day]["hours"]["users"][hour] = []
                day_data[day]["hours"]["users"][hour].append(user)


        with open("data_per_day_brainiac.json","w") as dest_file:
            json.dump(day_data, dest_file, sort_keys=True,indent=4, separators=(',',': '))
        dest_file.close()


        

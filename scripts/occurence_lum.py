#imports
import sys
import json
from pprint import pprint

if __name__ == "__main__":
    
    if len(sys.argv) < 2:
        print "Lacking json file and destination file"

    else:

        filename = sys.argv[1]

        with open(filename) as data_file:    
            data = json.load(data_file)


        ocurrence_lum_hour = {}

        for i in data.keys():
            a = data[i]["hours"]["luminosity"]
            for j in a.keys():
                b = a[j]
                for k in b:
                    if k == 0:
                        try:
                            ocurrence_lum_hour[j] += 1
                        except:
                            ocurrence_lum_hour[j] = 1



        with open("ocurrence_lum_hour.json","w") as dest_file:
            json.dump(ocurrence_lum_hour, dest_file, sort_keys=True, indent=4, separators=(',',': '))
        dest_file.close()


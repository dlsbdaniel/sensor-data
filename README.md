Scripts Descriptions
======================
In the scripts folder are the python scripts used to process the thingspeak 
data in JSON format. One set of scripts to 'clean' the json, leaving only 
the useful data, and another set of scripts to extract data, and group it 
according to our needs.


Scripts Usage
=================

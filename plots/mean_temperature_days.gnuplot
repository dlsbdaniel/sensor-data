#set grid
set xrange[-1:24]
set style fill solid 0.5 border
set xtic rotate by -45 scale 0
set xrange [-1:18]
set terminal postscript eps
set output 'temperature_days.eps'
plot 'source/temperature_plot' using 1:2:xtic(4) w boxes title "", 'source/temperature_plot' using 2:3 w yerrorbars title ""

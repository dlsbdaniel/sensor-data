#set grid
set xrange[-1:24]
set style fill solid 0.5 border
set terminal postscript eps
set output 'most_used_hours.eps'
plot 'source/luminosity_plot_hour' using 1:2:xtic(1) w boxes title ""

#set grid
set xrange[-1:24]
set style fill solid 0.5 border
set terminal postscript eps
set output 'mean_temperature_hour.eps'
plot 'source/mean_temp_hour' using 1:2:xtic(1) w boxes title ""

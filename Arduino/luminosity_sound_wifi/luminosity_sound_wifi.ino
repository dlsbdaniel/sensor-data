#include <ESP8266WiFi.h>

int lum_A = 2;
int lum_D = 4;
int sound_A = 3;
int sound_D = 5;

int lum_analog_value = 0;
int lum_digital_value = 0;
int sound_analog_value = 0;
int sound_digital_value = 0;

// replace with your channel’s thingspeak API key,
String apiKey = "YourThingspeakAPIKey";
const char* ssid = "YourSSID";
const char* password = "YourPassword";
const char* server = "api.thingspeak.com";

WiFiClient client;

void setup() {
    Serial.begin(115200);

    pinMode(lum_A, INPUT);
    pinMode(lum_D, INPUT);
    pinMode(sound_A, INPUT);
    pinMode(sound_D, INPUT);
    
    delay(10);
    //dht.begin();

    WiFi.begin(ssid, password);
    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
}
void loop() {
    //float demo = random(0,500);
    lum_analog_value = analogRead(lum_A);
    lum_digital_value = digitalRead(lum_D);
    sound_analog_value = analogRead(sound_A);
    sound_digital_value = digitalRead(sound_D);
    if (client.connect(server,80)) { // "184.106.153.149" or api.thingspeak.com
        String postStr = apiKey;
        postStr +="&field1=";
        postStr += String(lum_analog_value);
        postStr +="&field2=";
        postStr += String(lum_digital_value);
        postStr += "&field3=";
        postStr += String(sound_analog_value);
        postStr += "&field4=";
        postStr += String(sound_digital_value);
        postStr += "\r\n\r\n";
        client.print("POST /update HTTP/1.1\n");
        client.print("Host: api.thingspeak.com\n");
        client.print("Connection: close\n");
        client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
        client.print("Content-Type: application/x-www-form-urlencoded\n");
        client.print("Content-Length: ");
        client.print(postStr.length());
        client.print("\n\n");
        client.print(postStr);
        Serial.print("Luminosity Analog Value: ");
        Serial.println(lum_analog_value);
        Serial.print("Luminosity Digital Value: ");
        Serial.println(lum_digital_value);
        Serial.print("Sound Analog Value: ");
        Serial.println(sound_analog_value);
        Serial.print("Sound Digital Value: ");
        Serial.println(sound_digital_value);
        Serial.println("-------------------------------------\n");
        Serial.println("% send to Thingspeak");
    }
    client.stop();
    
    Serial.println("Waiting...");
    // thingspeak needs minimum 15 sec delay between updates
    delay(600000);
}

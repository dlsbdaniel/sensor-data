#include <DHT.h>
#include <ESP8266WiFi.h>

#define DHTPIN 2 // what pin we’re connected to
#define DHTTYPE DHT11

// replace with your channel’s thingspeak API key,
String apiKey = "YourThingspeakAPIKey";
const char* ssid = "YourSSID";
const char* password = "YourPassword";
const char* server = "api.thingspeak.com";

WiFiClient client;
DHT dht(DHTPIN, DHTTYPE);

void setup() {
    Serial.begin(115200);
    delay(10);
    //dht.begin();

    WiFi.begin(ssid, password);
    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
}
void loop() {
    float h = dht.readHumidity();
    float t = dht.readTemperature();
    if (client.connect(server,80)) { // "184.106.153.149" or api.thingspeak.com
        String postStr = apiKey;
        postStr +="&field1=";
        postStr += String(t);
        postStr +="&field2=";
        postStr += String(h);
        postStr += "\r\n\r\n";
        client.print("POST /update HTTP/1.1\n");
        client.print("Host: api.thingspeak.com\n");
        client.print("Connection: close\n");
        client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
        client.print("Content-Type: application/x-www-form-urlencoded\n");
        client.print("Content-Length: ");
        client.print(postStr.length());
        client.print("\n\n");
        client.print(postStr);
        
        Serial.println("\n % send to Thingspeak");
    }
    client.stop();

    Serial.print("Humidity: ");
        Serial.print(h);
        Serial.print("\n");
        Serial.print("Temperature: ");
        Serial.print(t);
    
    Serial.println("\n Waiting...");
    // thingspeak needs minimum 15 sec delay between updates
    delay(600000);
}

#include <DHT.h>

#define DHTPIN 2 // what pin we’re connected to
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

void setup() {
    Serial.begin(115200);
    delay(10);
    dht.begin();

}
void loop() {
    float h = dht.readHumidity();
    float t = dht.readTemperature();
    
    Serial.print("Humidity: ");
    Serial.print(h);
    Serial.print("\n");
    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.print("\n");

    delay(5000);
}
